from data.celonisClient import getCluster


class RawClusterLookup:

    lookupTable = {}

    def getKey(self, tableName, minpk, epsilon): 
      return f"{tableName}-{minpk}-{epsilon}"

    def getRawCluster(self, tableName, minpk, epsilon): 
      
      key = self.getKey(tableName, minpk, epsilon)

      if not key in list(RawClusterLookup.lookupTable.keys()):
        #Load Key
        RawClusterLookup.lookupTable[key] = getCluster(tableName, minpk, epsilon)
      return RawClusterLookup.lookupTable[key]  