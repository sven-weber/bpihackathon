from code import interact
from matplotlib.pyplot import table
from pycelonis import get_celonis
from pycelonis import pql
import pandas as pd
import json
import os

CELONIS_URL=os.getenv('CELONIS_URL')
CELONIS_API_KEY=os.getenv('CELONIS_API_KEY')

celonis = get_celonis(
    url=CELONIS_URL,
    api_token=CELONIS_API_KEY
)
#get clusteres traces and count
def getCluster(tableName, minpk, epsilon):
    activityTable = ""
    caseTable = ""
    if(tableName == "SAP P2P"):
        activityTable = '_CEL_P2P_ACTIVITIES_EN_parquet"."ACTIVITY_EN'
        caseTable = '_CEL_P2P_ACTIVITIES_EN_parquet"."_CASE_KEY'
    elif(tableName == "MobIS"):
        activityTable = 'mobis_challenge_log_2019_csv"."ACTIVITY'
        caseTable = 'mobis_challenge_log_2019_csv"."CASE'
    elif(tableName == "BPI 2017"):
        activityTable = 'BPI2017_offer_xes"."CONCEPT:NAME'
        caseTable = 'BPI2017_offer_xes"."CASE ID'

    datamodel = celonis.datamodels.find(tableName)

    clusterAndTraces = celonis.datamodels.find(tableName)
    clusterAndTraces = pql.PQL()
    clusterAndTraces += pql.PQLColumn('DISTINCT VARIANT("'+activityTable+'")', "Traces")
    clusterAndTraces += pql.PQLColumn('CLUSTER_VARIANTS(VARIANT("'+activityTable+'"),'+str(minpk)+','+str(epsilon)+')', "Clusters")

    tracesAndCount = celonis.datamodels.find(tableName)
    tracesAndCount = pql.PQL()
    tracesAndCount += pql.PQLColumn('DISTINCT VARIANT("'+activityTable+'")', "Traces")
    tracesAndCount += pql.PQLColumn('COUNT( DISTINCT "'+caseTable+'" )', "Count")

    return pd.merge(datamodel.get_data_frame(clusterAndTraces), datamodel.get_data_frame(tracesAndCount), on=["Traces"])

#gives a list of minpts values for clustering
def getClusterParamEstimation(tableName, epsilon):
    datamodel = celonis.datamodels.find(tableName)
    activityTable = ""
    if(tableName == "SAP P2P"):
        activityTable = '_CEL_P2P_ACTIVITIES_EN_parquet"."ACTIVITY_EN'
    elif(tableName == "MobIS"):
        activityTable = 'mobis_challenge_log_2019_csv"."ACTIVITY'
    elif(tableName == "BPI 2017"):
        activityTable = 'BPI2017_offer_xes"."CONCEPT:NAME'
    q = pql.PQL()
    q+= pql.PQLColumn('ESTIMATE_CLUSTER_PARAMS ( VARIANT("'+activityTable+'"), '+str(epsilon)+', 2, 15 )', "minpk est")
    return list(datamodel.get_data_frame(q)['minpk est'])
    

#get amount of clusters
def getClusterCount(clusterDataframe):
    return clusterDataframe["Clusters"].nunique()

#calculates the size of the different clusters
def getSizeOfClusters(clusterDataframe):
    clusterSizeDict = {}
    for cluster in list(clusterDataframe["Clusters"]):
        clusterSizeDict[cluster] = sum(list(clusterDataframe.query("Clusters=="+str(cluster))["Count"]))
    return clusterSizeDict

def getClusterAndGlobalSize(clusterDataframe, clusterID):
    return [sum(list(clusterDataframe["Count"])), sum(list(clusterDataframe.query("Clusters=="+str(clusterID))["Count"]))]


#calculate intersection of directly-follows relation for every cluster with every cluster
def getRelativeHamilltonDistance(clusterDataframe):
    #calculate directly follows relation for every cluster
    directlyFollowsDict = {}
    for cluster in list(clusterDataframe["Clusters"].drop_duplicates()):
        directlyFollowsDict[cluster] = []
    for i in range(0, len(clusterDataframe)):
        dataframe = clusterDataframe.iloc[i]
        activities = dataframe["Traces"].split(",")
        for j in range(1, len(activities)):
            directlyFollowsDict[dataframe["Clusters"]].append([activities[j-1], activities[j]])
    #calculate intersection of every cluster with every other cluster
    intersectDict = {}
    def intersection(lst1, lst2):
        lst3 = [value for value in lst1 if value in lst2]
        return lst3
    keys = list(directlyFollowsDict.keys())
    for currCluster in keys:
        for targetCluster in keys[keys.index(currCluster):]:
            if(currCluster != targetCluster):
                numberOfDF = len(directlyFollowsDict[currCluster]) + len(directlyFollowsDict[targetCluster])
                intersectDict[str(currCluster)+"x"+str(targetCluster)] = len(intersection(directlyFollowsDict[currCluster], directlyFollowsDict[targetCluster]))/numberOfDF
    return intersectDict

#returns dict with list of traces for every cluster
def getClusterTraceDict(clusterDataframe):
    clusterDfDict = {}
    for cluster in list(set(clusterDataframe["Clusters"])):
        clusterDfDict[cluster] = []
    for i in range(0, len(clusterDataframe)):  
        dataframe = clusterDataframe.iloc[i]
        clusterDfDict[dataframe["Clusters"]].append(dataframe["Traces"])
    return clusterDfDict


#finds happy paths
def getHappyPathsWithFraction(clusterDataframe):
    happyPathDict = {}
    for cluster in set(clusterDataframe["Clusters"]):
        sortedCluster = clusterDataframe.query('Clusters == '+str(cluster)).sort_values(by=['Count'], ascending=False)
        happyPathDict[sortedCluster.iloc[0]["Clusters"]] = [[el.strip() for el in (sortedCluster.iloc[0]["Traces"]).split(",")], sortedCluster.iloc[0]["Count"]/sortedCluster["Count"].sum()]
    return happyPathDict

#returnes name of attributes of table with tableName
def getAttributes(tableName):
    attributes = []
    attribute_dict = json.load(open(os.path.dirname(os.path.realpath(__file__)) + "/attributes.json", "r", encoding="utf-8"))
    for table in list(attribute_dict[tableName].keys()):
        attributes += attribute_dict[tableName][table]
    return attributes

def getAttributeDistributionCluster(tableName, minpk, epsilon, attributeName):
    #determine case and activity table
    activityTable = ""
    if(tableName == "SAP P2P"):
        activityTable = '_CEL_P2P_ACTIVITIES_EN_parquet"."ACTIVITY_EN'
    elif(tableName == "MobIS"):
        activityTable = 'mobis_challenge_log_2019_csv"."ACTIVITY'
    elif(tableName == "BPI 2017"):
        activityTable = 'BPI2017_offer_xes"."CONCEPT:NAME'
    attribute_dict = json.load(open(os.path.dirname(os.path.realpath(__file__)) + "/attributes.json", "r", encoding="utf-8"))
    sheetName = ""
    for table in attribute_dict[tableName]:
        if attributeName in list(attribute_dict[tableName][table]):
            sheetName = table 
    #load and merge attribute tables
    datamodel = celonis.datamodels.find(tableName)
    attributeDistrCluster = pql.PQL()
    attributeDistrCluster += pql.PQLColumn('"'+sheetName+'"."'+attributeName+'"', "Attribute")
    attributeDistrCluster += pql.PQLColumn('CLUSTER_VARIANTS(VARIANT("'+activityTable+'"),'+str(minpk)+','+str(epsilon)+')', "Clusters")
    attributeDistrCluster += pql.PQLColumn('COUNT("'+sheetName+'"."'+attributeName+'")', "AttributeCountCluster")
    return datamodel.get_data_frame(attributeDistrCluster)

def getAttributeDistributionAll(tableName, attributeName):
    attribute_dict = json.load(open(os.path.dirname(os.path.realpath(__file__)) + "/attributes.json", "r", encoding="utf-8"))
    sheetName = ""
    for table in attribute_dict[tableName]:
        if attributeName in list(attribute_dict[tableName][table]):
            sheetName = table 
    datamodel = celonis.datamodels.find(tableName)
    attributeDistrAll = pql.PQL()
    attributeDistrAll += pql.PQLColumn('"'+sheetName+'"."'+attributeName+'"', "Attribute")
    attributeDistrAll += pql.PQLColumn('COUNT("'+sheetName+'"."'+attributeName+'")', "AttributeCountGlobal")
    return datamodel.get_data_frame(attributeDistrAll)

def getAttributeInformation(tableName, minpk, epsilon, attributeName, clusterID):
    clusterAttrDF = getAttributeDistributionCluster(tableName, minpk, epsilon, attributeName).query("Clusters == "+str(clusterID))
    globalAttrDF = getAttributeDistributionAll(tableName, attributeName)
    clusterCount = sum(list(clusterAttrDF["AttributeCountCluster"]))
    globalCount = sum(list(globalAttrDF["AttributeCountGlobal"]))
    return (pd.merge(clusterAttrDF,globalAttrDF,on=["Attribute"]).sort_values(by='AttributeCountCluster', ascending=True)).append({'Attribute':"Counts", 'AttributeCountCluster':clusterCount, "AttributeCountGlobal":globalCount}, ignore_index=True).iloc[::-1]

def getAverageThroughputTime(tableName, minpk, epsilon, clusterID):
    datamodel = celonis.datamodels.find(tableName)
    timestampTable = ""
    activityTable = ""
    if(tableName == "SAP P2P"):
        timestampTable = '_CEL_P2P_ACTIVITIES_EN_parquet"."EVENTTIME'
        activityTable = '_CEL_P2P_ACTIVITIES_EN_parquet"."ACTIVITY_EN'
    elif(tableName == "MobIS"):
        timestampTable = 'mobis_challenge_log_2019_csv"."END'
        activityTable = 'mobis_challenge_log_2019_csv"."ACTIVITY'
    elif(tableName == "BPI 2017"):
        timestampTable = 'BPI2017_offer_xes"."TIME:TIMESTAMP'
        activityTable = 'BPI2017_offer_xes"."CONCEPT:NAME'
    q = pql.PQL()
    q+= pql.PQLColumn("AVG(CALC_THROUGHPUT(ALL_OCCURRENCE['Process Start'] TO ALL_OCCURRENCE['Process End'], REMAP_TIMESTAMPS("+'"'+timestampTable+'"'+", DAYS)))", "avgthroughput_all")
    averageThroughputAll = list(datamodel.get_data_frame(q)['avgthroughput_all'])[0]
    q = pql.PQL()
    q += pql.PQLColumn("AVG(CALC_THROUGHPUT(ALL_OCCURRENCE['Process Start'] TO ALL_OCCURRENCE['Process End'], REMAP_TIMESTAMPS("+'"'+timestampTable+'"'+", DAYS)))", "avgthroughput_clusters")
    q += pql.PQLColumn('CLUSTER_VARIANTS(VARIANT("'+activityTable+'"),'+str(minpk)+','+str(epsilon)+')', "Clusters")
    df = datamodel.get_data_frame(q)
    res = {"overall_time": averageThroughputAll} 
    for i in range(len(list(df["Clusters"]))):
        if(df["Clusters"][i] == clusterID):
            res[df["Clusters"][i]] = df["avgthroughput_clusters"][i]
    return res

clusterDataframe = getCluster('SAP P2P', 144, 2)
print(getSizeOfClusters(clusterDataframe))
#print(getClusterParamEstimation("SAP P2P", 2))
#print(getRelativeHamilltonDistance(clusterDataframe))
#print(getAverageThroughputTime("BPI 2017", 1000, 2, 3))
