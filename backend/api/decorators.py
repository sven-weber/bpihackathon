from functools import wraps
from flask import request
from flask_restful import abort

API_TOKEN = "hackathon"

def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 'Token' in request.headers:
            token = request.headers['Token']
            if (token == API_TOKEN):
                #Return function result
                return func(*args, **kwargs) 
        #Unauthenticated     
        abort(401)
    return wrapper