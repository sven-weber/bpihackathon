from api.decorators import authenticate
from data.celonisClient import getClusterAndGlobalSize
from data.celonisClient import getSizeOfClusters
from model.clusterDetails import ClusterDetails
from data.rawClusterLookup import RawClusterLookup
from data.celonisClient import getHappyPathsWithFraction, getAttributes, getAverageThroughputTime
from flask_restful import Resource, request

class ClusterDetailApi(Resource):
    method_decorators = [authenticate]
    def get(self, cluster_id):
        #Get Query Parameter
        args = request.args
        tablename = args.get('tableName', type=str)
        minpk = args.get('minpk', type=int)
        epsilon = args.get('epsilon', type=int)
        if None in (tablename, minpk, epsilon):
            return 'bad request, please provide tableName, minpk and epsilon', 400

        id = int(cluster_id)

        clustersRaw = RawClusterLookup().getRawCluster(tablename, minpk, epsilon)
        data = getHappyPathsWithFraction(clustersRaw)
        attributes = getAttributes(tablename)
        troughputtime = getAverageThroughputTime(tablename, minpk, epsilon, id)
        clusterAndGlobalSize = getClusterAndGlobalSize(clustersRaw, id)

        return ClusterDetails(id, data[id][0], data[id][1], attributes, troughputtime[id], troughputtime['overall_time'], clusterAndGlobalSize[1],clusterAndGlobalSize[0])