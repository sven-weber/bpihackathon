from api.decorators import authenticate
from data.rawClusterLookup import RawClusterLookup
from data.celonisClient import getCluster, getSizeOfClusters, getRelativeHamilltonDistance
from model.cluster import Cluster
from flask_restful import Resource, request

class ClusterApi(Resource):
    method_decorators = [authenticate]
    def get(self):
        #Get Query Parameter
        args = request.args
        tablename = args.get('tableName', type=str)
        minpk = args.get('minpk', type=int)
        epsilon = args.get('epsilon', type=int)
        if None in (tablename, minpk, epsilon):
            return 'bad request, please provide tableName, minpk and epsilon', 400

        #Build cluster objects
        clustersRaw = RawClusterLookup().getRawCluster(tablename, minpk, epsilon)
        sizeOfClusters = getSizeOfClusters(clustersRaw)
        clusters = {}

        for key in list(sizeOfClusters.keys()): 
            clusters[str(key)] = Cluster(str(key), int(sizeOfClusters[key]))
        
        #Add overlaps
        intersect = getRelativeHamilltonDistance(clustersRaw)
        for key in list(intersect.keys()): 
            keylist = key.split('x')
            clusters[keylist[0]].addIntersection(keylist[1], intersect[key])
            clusters[keylist[1]].addIntersection(keylist[0], intersect[key])
        
        return list(clusters.values())