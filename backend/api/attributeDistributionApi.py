from api.decorators import authenticate
from model.attributeDistribution import AttributeDistribution
from model.clusterDetails import ClusterDetails
from data.rawClusterLookup import RawClusterLookup
from data.celonisClient import getAttributeInformation, getHappyPathsWithFraction, getAttributes
from flask_restful import Resource, request

class AttributeDistributionApi(Resource):
    method_decorators = [authenticate]

    def get(self, cluster_id, attribute_name):
        #Get Query Parameter
        args = request.args
        tablename = args.get('tableName', type=str)
        minpk = args.get('minpk', type=int)
        epsilon = args.get('epsilon', type=int)

        if None in (tablename, minpk, epsilon):
            return 'bad request, please provide tableName, minpk and epsilon', 400

        info = getAttributeInformation(tablename, minpk, epsilon, attribute_name, int(cluster_id))
        res = []

        for i in range(0, len(info)):
          data =  info.iloc[i]
          res.append(AttributeDistribution(data['Attribute'], int(data['AttributeCountCluster']), int(data['AttributeCountGlobal'])))

        return res