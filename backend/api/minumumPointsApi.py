from api.decorators import authenticate
from data.celonisClient import getClusterParamEstimation
from flask_restful import Resource, request

class MinimumPointsApi(Resource):
    method_decorators = [authenticate]
    def get(self):
        #Get Query Parameter
        args = request.args
        tablename = args.get('tableName', type=str)
        epsilon = args.get('epsilon', type=int)

        if None in (tablename, epsilon):
            return 'bad request, please provide tableName and epsilon', 400
        
        return list(reversed(getClusterParamEstimation(tablename, epsilon)))

