
from api.clusterApi import ClusterApi
from api.minumumPointsApi import MinimumPointsApi
from api.clusterDetailApi import ClusterDetailApi
from api.attributeDistributionApi import AttributeDistributionApi
from flask import Flask
from flask_restful import Api
from flask_cors import CORS

PORT = 8080

def run():
    app = Flask(__name__)
    CORS(app)

    api = Api(app)
    
    #Configure
    api.add_resource(ClusterApi, '/clusters')
    api.add_resource(ClusterDetailApi, '/clusters/<string:cluster_id>')
    api.add_resource(MinimumPointsApi, '/minimumpoints')
    api.add_resource(AttributeDistributionApi, '/clusters/<string:cluster_id>/<string:attribute_name>')
    
    #Run 
    app.run(host='0.0.0.0', port=PORT, debug=False)   

    print(f"Server running on port {PORT}")