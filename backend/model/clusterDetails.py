import json_fix

class ClusterDetails:

    def __init__(self, name, happyPath, happyPathFraction : float, attributes, clusterAVGTroughputTime, globalAVGTroughputTime, clusterSize, globalSize):
        self.name = name
        self.happyPath = happyPath
        self.happyPathFraction = happyPathFraction
        self.attributes = attributes
        self.clusterAVGTroughputTime = clusterAVGTroughputTime 
        self.globalAVGTroughputTime = globalAVGTroughputTime
        self.clusterSize = clusterSize
        self.globalSize = globalSize

    def __json__(self):
        return self.__dict__