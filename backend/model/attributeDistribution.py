import json_fix

class AttributeDistribution:

    def __init__(self, attribute, attributeCountCluster, attributeCountGlobal): 
        self.attribute = attribute
        self.attributeCountCluster = attributeCountCluster
        self.attributeCountGlobal = attributeCountGlobal

    def __json__(self):
        return self.__dict__