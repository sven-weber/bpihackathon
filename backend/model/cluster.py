import json_fix

class Cluster:

    def __init__(self, name : str, size : int): 
        self.name = name
        self.size = size
        self.intersections = []

    def addIntersection(self, name : str, size : int): 
        self.intersections.append({"name": name, "size": size})

    def __json__(self):
        return self.__dict__