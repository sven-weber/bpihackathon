import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularMaterialModule } from './material.module';
import { MainViewComponent } from './components/main-view/main-view.component';
import { ClusterDetailsSideviewComponent } from './components/cluster-details-sideview/cluster-details-sideview.component';
import { HttpClientModule } from '@angular/common/http';
import { AttributeExplorerComponent } from './components/cluster-details-sideview/attribute-explorer/attribute-explorer.component';
import { ClusterOverviewComponent } from './components/cluster-details-sideview/cluster-overview/cluster-overview.component';
import { ClusterComparisonComponent } from './components/cluster-details-sideview/cluster-comparison/cluster-comparison.component';

@NgModule({
  declarations: [
    AppComponent,
    MainViewComponent,
    ClusterDetailsSideviewComponent,
    AttributeExplorerComponent,
    ClusterOverviewComponent,
    ClusterComparisonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularMaterialModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
