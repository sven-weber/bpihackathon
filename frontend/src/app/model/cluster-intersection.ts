export interface ClusterIntersection {
    name: string;
    size: number;
}