export interface ClusterDetails {
  name: string;
  happyPath: string[];
  happyPathFraction: number;
  clusterAVGTroughputTime: number;
  globalAVGTroughputTime: number;
  attributes: string[];
  clusterSize: number;
  globalSize: number;
}