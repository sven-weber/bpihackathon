import { Circle } from "two.js/src/shapes/circle";

export interface ClusterDrawData {
  x: number;
  y: number; 
  radius: number;
  circle?: Circle;
  name: string;
  color: string;
}