export interface AttributeDistribution {
  attribute: string; 
  attributeCountCluster: number; 
  attributeCountGlobal: number;
}