import { ClusterIntersection } from "./cluster-intersection";

export interface Cluster {
    name: string; 
    size: number; 
    intersections: ClusterIntersection[]
}