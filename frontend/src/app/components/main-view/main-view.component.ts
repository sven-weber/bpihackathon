import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, OnInit, Output, ViewChild, ɵɵngDeclareClassMetadata } from '@angular/core';
import { MatGridTileHeaderCssMatStyler } from '@angular/material/grid-list';
import { Cluster } from 'src/app/model/cluster';
import { ApiClientService } from 'src/app/services/api-client.service';
import * as _ from 'lodash';
import Two from 'two.js';
import { ClusterDrawData } from 'src/app/model/cluster-draw-data';
import { ClusterIntersection } from 'src/app/model/cluster-intersection';
import { Circle } from 'two.js/src/shapes/circle';
import { color } from '@amcharts/amcharts5';
import { LineSeries } from '@amcharts/amcharts5/xy';
import { throwIfEmpty } from 'rxjs';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss']
})
export class MainViewComponent implements AfterViewInit {

  @ViewChild('canvas') canvas!: ElementRef;
  
  public isLoading = true;
  private clusters: Cluster[] = [];
  @Output() clustersChange = new EventEmitter<Cluster[]>();
  private maxCircleSize : number = 200;
  private minCircleSize : number = 0;
  private lineSize: number = 10;
  private two!: Two;
  private minimumIntersection: number = 0;
  private clusterDrawData: { [id: string]: ClusterDrawData } = {};
  private colors: string[] =
    [ "#003b49", "#1d4289", "#dc582a", "#1b365d", "#5d3754",
      "#007a78"]
  private colorIndex: number = 0;
  private selectedCluster?: ClusterDrawData = undefined;
  @Output() selectedClusterChange = new EventEmitter<string>();
  @Output() selectedClusterColorChange = new EventEmitter<string>();
  private listenerPointerdown: any = undefined;
  private listenerPointermove: any = undefined;
  private lines: any[] = [];
  private canvasClusters: any[] = [];
  private canvasText: any[] = [];
  private background: any = undefined;
  private foreground: any = undefined;

  constructor(private api: ApiClientService) { }
  
  ngAfterViewInit(): void {
    
    //shuffle the colors
    this.colors = _.shuffle(this.colors);

    this.two = new Two({
      type: Two.Types.canvas,
      width: this.canvas.nativeElement.offsetWidth - 70,
      height: this.canvas.nativeElement.offsetHeight - 60,
      autostart: true
    }).appendTo(this.canvas.nativeElement);

    this.api.minPtsChangeEvent.subscribe(() => {
       //Clear canvas, remove old listeners
       this.two.clear();
       this.selectedClusterChange.emit("");
       this.selectedCluster = undefined;
       
       if (this.listenerPointerdown)
       {
         window.removeEventListener('pointerdown', this.listenerPointerdown);
       }
       if (this.listenerPointermove)
       {
         window.removeEventListener('pointermove', this.listenerPointermove);
       }
 
       this.isLoading = true;
       this.fetchClusters();
    })

    this.api.connectionLevelChangeEvent.subscribe((newLevel) => 
    {
      this.minimumIntersection = newLevel / 100; 

      this.isLoading = true;
      this.cleanLines();
      this.cleanClusters();
      this.drawLines();
      this.drawClusters();
      this.isLoading = false;
    })
  }
  
  getSelectedCluster() : string
  {
    if (this.selectedCluster)
    {
      return this.selectedCluster.name;  
    }
    return "";
  }

  getClusterColor() : string
  {
    if (this.colorIndex >= this.colors.length)
    {
      this.colorIndex = 0;  
    }

    let color = this.colors[this.colorIndex];
    this.colorIndex += 1;
    return color;
  }

  fetchClusters(): void {
    this.api.getClusters().subscribe(result => {
      this.clusters = _.sortBy(result, x => x.name);
      this.clustersChange.emit(_.sortBy(result, x => Number.parseInt(x.name)));
      console.log("Got clusters")
      this.isLoading = false;
      this.draw();
    });
  }

  //See https://www.geeksforgeeks.org/check-two-given-circles-touch-intersect/
  doCircleCollide(first : ClusterDrawData , second: ClusterDrawData ) : boolean
  {
      let distSq = (first.x - second.x) * (first.x - second.x) + (first.y - second.y) * (first.y - second.y);
      let radSumSq = (first.radius + second.radius) * (first.radius + second.radius);
      return distSq <= radSumSq;
  }

  doesClusterCollide(currentClusters: ClusterDrawData[], x: number, y: number, radius: number) : boolean
  {
    for (let cluster of currentClusters)
    {
      if (this.doCircleCollide(cluster, { x: x, y: y, radius: radius, name: "", color: "" }))
      {
        return true;
      }
    }
    return false;
  }

  clusterClicked(self : MainViewComponent, drawData : ClusterDrawData, cluster: Cluster): void
  {   
    console.log("Selected cluster: " + cluster.name);
    //Draw selected item differently
    if (drawData.circle)
    {
      drawData.circle.stroke = '#5cfe50';
      drawData.circle.linewidth = 5;
    }

    //Undraw old item
    if (self.selectedCluster && self.selectedCluster.circle)
    {
      self.selectedCluster.circle.noStroke();
    }

    //Update Selected, expect same was selected again -> deselected
    if (self.selectedCluster != drawData)
    {
      self.selectedCluster = drawData;
      self.selectedClusterChange.emit(drawData.name);
      self.selectedClusterColorChange.emit(drawData.color);
    } else 
    {
      self.selectedCluster = undefined;
      self.selectedClusterChange.emit("");
    }
    self.two.update();
  }

  cleanLines(): void
  {
    for (let line of this.lines)
    {
      this.two.remove(line); 
    }
  }

  cleanClusters(): void{
    for (let cluster of this.canvasClusters)
    {
      this.two.remove(cluster); 
    }
    for (let text of this.canvasText)
    {
      this.two.remove(text);  
    }
  }

  drawLines(): void
  {
    //Create lines
    this.lines = []
    for (let cluster of this.clusters)
    {
      for (let intersection of cluster.intersections)
      {
        if (intersection.size >= this.minimumIntersection)
        {
          let first = this.clusterDrawData[intersection.name]
          let second = this.clusterDrawData[cluster.name]
          let line = this.two.makeLine(first.x, first.y, second.x, second.y);
          this.lines.push(line);
          line.stroke = "#FFFFFF"
          line.linewidth = intersection.size * this.lineSize;
          if (intersection.size > 0.75)
          {
            line.stroke = "#5cfe50"
          }
          if (intersection.size > 0.25 && intersection.size <= 0.5)
          {
            line.linewidth = line.linewidth / 2;
            line.dashes = [13, 13];
          }
          if (intersection.size <= 0.25)
          {
            line.linewidth = line.linewidth / 2;
            line.dashes = [13, 13];
            line.opacity = 0.5;
          }
        }          
      }
    }
  }

  drawClusters(): void {
    //Draw clusters
    this.canvasClusters = []
    for (let cluster of this.clusters)
    {
      let drawData = this.clusterDrawData[cluster.name];
      drawData.circle = this.two.makeCircle(drawData.x, drawData.y, drawData.radius);
      drawData.circle.fill = drawData.color;
      drawData.circle.noStroke();
      this.canvasClusters.push(drawData)
      let text = this.two.makeText(cluster.name, drawData.x, drawData.y, { size: Math.min(75, Math.min(drawData.radius, 125)), stroke: "#FFFFFF", fill: "#FFFFFF" });
      this.canvasText.push(text);
    }
  }

  draw(): void {
    
    //Get the Maximum cluster and intersection size
    console.log("Get max Cluster");
    let maxCluster = _.maxBy(this.clusters, function (c) { return c.size });
    console.log(maxCluster);
    console.log("Drawing");

    if (maxCluster)
    {
      let maxClusterSize = maxCluster.size;
      this.clusterDrawData = {};
      //Create data per cluster
      for (let cluster of this.clusters)
      {
        let radius = Math.max(this.minCircleSize, (cluster.size / maxClusterSize) * this.maxCircleSize)
        radius += 15;

        //Check collision with other circles
        let x: number = this.two.width * 0.5; 
        let y : number = this.two.height * 0.5;
        if (_.keys(this.clusterDrawData).length > 0)
        {
          do
          {
            x = Math.random() * this.two.width; 
            y = Math.random() * this.two.height;
          } while ((x - radius <= 0 || x + radius >= this.two.width ||
                  y - radius <= 0 || y + radius >= this.two.height - 35 ) ||
                  this.doesClusterCollide(_.values(this.clusterDrawData), x, y, radius)) 
        }
        
        let color = this.getClusterColor();
        this.clusterDrawData[cluster.name] = { x: x, y: y, radius: radius, circle: undefined, name: cluster.name, color};
      }

      //Draw Lines
      this.drawLines();

      //Draw Clusters
      this.drawClusters();

      //Draw legend
      let paddingBottom = 20;
      let firstLine = this.two.makeLine(30, this.two.height - paddingBottom, 120, this.two.height - paddingBottom);
      firstLine.stroke = "#5cfe50"
      firstLine.linewidth = 8;
      this.two.makeText(">75% overlap", 167, this.two.height - paddingBottom, {  fill: "#FFFFFF" })
      
      let secondLine = this.two.makeLine(230, this.two.height - paddingBottom, 340, this.two.height - paddingBottom);
      secondLine.stroke = "#FFFFFF"
      secondLine.linewidth = 8;
      this.two.makeText("75%-50% overlap", 398, this.two.height - paddingBottom, { fill: "#FFFFFF" })
      
      let thirdLine = this.two.makeLine(470, this.two.height - paddingBottom, 610, this.two.height - paddingBottom);
      thirdLine.stroke = "#FFFFFF"
      thirdLine.dashes = [13, 13];
      thirdLine.linewidth = 6;
      this.two.makeText("49%-25% overlap", 668, this.two.height - paddingBottom, { fill: "#FFFFFF" })

      let fourthLine = this.two.makeLine(740, this.two.height - paddingBottom, 870, this.two.height - paddingBottom);
      fourthLine.stroke = "#FFFFFF"
      fourthLine.dashes = [13, 13];
      fourthLine.linewidth = 4;
      fourthLine.opacity = 0.5;
      this.two.makeText("<25% overlap", 902, this.two.height - paddingBottom, { fill: "#FFFFFF" })
    
      this.two.update();
      
      let cluster = this.clusters;
      let drawData = this.clusterDrawData;
      let clickCallBack = this.clusterClicked;
      let self = this; 

      let collisionDectection = (e : MouseEvent, two: Two, funcCollision : any, funcNoCollosion?: any) => 
      {
        let x = e.x;
        let y = e.y -60;

        let collision = false;
        for (let key of cluster)
        {
          let data = drawData[key.name];
          //Check if click is in cluster
          let dist = Math.sqrt((data.x - x) * (data.x - x) + (data.y - y) * (data.y - y)); 
          if (dist < data.radius && data.circle)
          {
            funcCollision(self, data, key);
            collision = true;
            break;
          }
        }

        if (funcNoCollosion && !collision)
        {
          funcNoCollosion();
        }
      }

      this.listenerPointerdown = (e : MouseEvent)  =>
      {
        collisionDectection(e,this.two, clickCallBack);
      }
      window.addEventListener('pointerdown', this.listenerPointerdown );

      this.listenerPointermove = (e : MouseEvent) =>
      {
        collisionDectection(e, this.two, () => 
        {
          document.body.style.cursor = 'pointer'
        }, () => 
        {
          document.body.style.cursor = 'default'
        });
      }
      window.addEventListener('pointermove', this.listenerPointermove);
    }
    /**
      this.clusters.
      
  
      var x = 300;
      var y = 350;
      var x2 = 200;
      var y2 = 250;

      var line = two.makeLine(x, y, x2, y2);
      line.linewidth = 5;

      var radius = 50;
      

      var radius2 = 50;
      var circle2 = two.makeCircle(x2, y2, radius2);

      // The object returned has many stylable properties:
      circle.fill = '#FF8000';
      // And accepts all valid CSS color:
      circle.stroke = 'orangered';
      circle.linewidth = 5;

      circle2.fill = 'red'
      circle.stroke = 'black';
      circle.linewidth = 5;

      // Don’t forget to tell two to draw everything to the screen
      two.update();
    */
  }
  
}
