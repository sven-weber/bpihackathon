import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClusterDetailsSideviewComponent } from './cluster-details-sideview.component';

describe('ClusterDetailsSideviewComponent', () => {
  let component: ClusterDetailsSideviewComponent;
  let fixture: ComponentFixture<ClusterDetailsSideviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClusterDetailsSideviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterDetailsSideviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
