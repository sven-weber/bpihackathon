import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as _ from 'lodash';
import { ClusterDetails } from 'src/app/model/cluster-detail';
import Two from 'two.js';

@Component({
  selector: 'app-cluster-overview',
  templateUrl: './cluster-overview.component.html',
  styleUrls: ['./cluster-overview.component.scss']
})
export class ClusterOverviewComponent implements OnChanges, AfterViewInit {

  @Input() clusterDetails: ClusterDetails | null = null;

  @ViewChild('canvas') canvas!: ElementRef;
  private two!: Two;
  private padding: number = 50;

  constructor() { }


  ngAfterViewInit()
  {
    this.draw();
  }

  getRectangleWidth(content: string)
  {
    return content.length * 8;
  }



  draw(): void {

    if(this.clusterDetails && this.canvas) {

      if (this.two)
      {
        this.two.clear();
        this.canvas.nativeElement.innerHTML = "";
      }

      let data = this.clusterDetails?.happyPath;

      //Calculate the space between rectangles
      let x = this.canvas.nativeElement.offsetWidth * 0.5; //centered 
      let size = data.length;
      let itemHeight = 40;
      let itemSpace = itemHeight;
      let itemWidth = _.max(data.map(x => this.getRectangleWidth(x))) || 0;

      //Calculate coordinates
      let currentY = this.padding;
      let activityY: { [id: string]: number } = {};
      for (let activity in data)
      {
        activityY[activity] = currentY;
        currentY += itemSpace + itemHeight;
      }

      //Set new max height
      let maxHeight = this.padding * 2 + (size * itemHeight) + ((size-1) * itemSpace);
      
      //Load canvas
      this.two = new Two({
        type: Two.Types.canvas,
        width: this.canvas.nativeElement.offsetWidth,
        height: maxHeight,
        autostart: true
      }).appendTo(this.canvas.nativeElement);

      //Draw the lines
      for (let i = 0; i < data.length - 1; i++)
      {
        let line = this.two.makeArrow(x, activityY[i], x, activityY[i + 1] - (itemHeight/2) -2, 10);
        line.stroke = "#5cfe50"
        line.linewidth = 3;
      }

      //Draw the activities Path
      for (let activity in data)
      {
        let rectangle = this.two.makeRoundedRectangle(x, activityY[activity], itemWidth, itemHeight, 20);
        rectangle.noStroke();
        rectangle.fill = "#525252"
        this.two.makeText(data[activity], x, activityY[activity], {fill: "white"});
      }

      this.two.update();
    }
  } 

  ngOnChanges(): void {
    this.draw();
  }
}
