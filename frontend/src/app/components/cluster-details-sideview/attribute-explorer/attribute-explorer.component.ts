import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ClusterDetails } from 'src/app/model/cluster-detail';
import { ApiClientService } from 'src/app/services/api-client.service';

import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import am5themes_Dark from "@amcharts/amcharts5/themes/Dark";

@Component({
  selector: 'app-attribute-explorer',
  templateUrl: './attribute-explorer.component.html',
  styleUrls: ['./attribute-explorer.component.scss']
})
export class AttributeExplorerComponent implements OnInit, OnChanges, AfterViewInit {


  @Input() color: string = "#FFFFFF";
  @Input() clusterDetails: ClusterDetails | null = null;
  

  public loading = false;

  private fractionRankingCluster: number[] = [];
  private fractionRankingGlobal: number[] = [];
  private attributeNames: string[] = [];
  private root!: am5.Root;
  private data: {attributeName: string; fractionRankingCluster: number; fractionRankingGlobal: number}[] = [];

  selectedAttribute: string = "";
  
  constructor(private api: ApiClientService) { }

  ngOnInit(): void {
    console.log(this.clusterDetails)
  }

  ngAfterViewInit(): void {
    
  }

  ngOnChanges(): void {
    this.selectedAttribute = "";
    if(this.clusterDetails === null) {
      this.root.container.children.clear();
    }
  }

  onSelectionChange(): void {
    let data: {attributeName: string; fractionRankingCluster: number; fractionRankingGlobal: number}[] = [];
    if(this.clusterDetails != null){
      this.loading = true;
      this.api.getAttributeDistribution(this.clusterDetails.name, this.selectedAttribute).subscribe(result => {

        
        let rankLength = result.length >= 5 ? 5 : result.length;
        for (let i = 1; i <= rankLength-1; i++) {
          data.push({
            attributeName: result[i].attribute, 
            fractionRankingCluster: (result[i].attributeCountCluster / result[0].attributeCountCluster) * 100, 
            fractionRankingGlobal: (result[i].attributeCountGlobal / result[0].attributeCountGlobal) * 100
          });
        }
        this.loading = false;
        this.drawChart(data);
      });

    console.log(data);
    }
  }


  public drawChart(data: {attributeName: string; fractionRankingCluster: number; fractionRankingGlobal: number}[]): void {
   
    if(!this.root) {
      this.root = am5.Root.new("chartdiv");
      this.root.setThemes([am5themes_Dark.new(this.root)]);
    } else {
      this.root.container.children.clear();
    }

  

    let chart = this.root.container.children.push( 
      am5xy.XYChart.new(this.root, {
        panY: false,
        layout: this.root.verticalLayout
      }) 
    );

    

   

    // Create Y-axis
    let yAxis = chart.yAxes.push(
      am5xy.ValueAxis.new(this.root, {
        renderer: am5xy.AxisRendererY.new(this.root, {})
      })
    );



    // Create X-Axis
    let xAxis = chart.xAxes.push(
      am5xy.CategoryAxis.new(this.root, {
        renderer: am5xy.AxisRendererX.new(this.root, {}),
        categoryField: "attributeName"
      })
    );
    xAxis.data.setAll(data);

    // Create series
    let series1 = chart.series.push(
      am5xy.ColumnSeries.new(this.root, {
        name: "Attribute Fraction in Cluster",
        xAxis: xAxis,
        yAxis: yAxis,
        valueYField: "fractionRankingCluster",
        categoryXField: "attributeName"
      })
    );
    series1.set("fill", am5.color(this.color));
    series1.data.setAll(data);

    let series2 = chart.series.push(
      am5xy.ColumnSeries.new(this.root, {
        name: "Attribute Fraction Overall",
        xAxis: xAxis,
        yAxis: yAxis,
        valueYField: "fractionRankingGlobal",
        categoryXField: "attributeName"
      })
    );
    series2.set("fill", am5.color("#FFFFFF"))
    series2.data.setAll(data);

    // Add legend
    let legend = chart.children.push(am5.Legend.new(this.root, {}));
    legend.data.setAll(chart.series.values);

    // Add cursor
    chart.set("cursor", am5xy.XYCursor.new(this.root, {}));

  }
  

}
