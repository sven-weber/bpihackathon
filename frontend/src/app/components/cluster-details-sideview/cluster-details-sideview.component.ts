import { EventListenerFocusTrapInertStrategy } from '@angular/cdk/a11y';
import { ThisReceiver } from '@angular/compiler';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Cluster } from 'src/app/model/cluster';
import { ClusterDetails } from 'src/app/model/cluster-detail';
import { ApiClientService } from 'src/app/services/api-client.service';

@Component({
  selector: 'app-cluster-details-sideview',
  templateUrl: './cluster-details-sideview.component.html',
  styleUrls: ['./cluster-details-sideview.component.scss']
})
export class ClusterDetailsSideviewComponent implements OnInit, OnChanges {

  @Input() color: string = "#FFFFFF";
  @Input() clusterId: string = "";
  @Input() clusters!: Cluster[];
  loading = false;
  tabreset = 0;


  clusterDetails: ClusterDetails | null = null;

  constructor(public api: ApiClientService) { }

  ngOnInit(): void {
  
  }

  ngOnChanges(): void {
    this.tabreset = 0;
    if(this.clusterId !== "") {
      this.loading = true;
      this.api.getClusterDetails(this.clusterId).subscribe(result => {
        this.loading = false;
        this.clusterDetails = result;
      });
    } else {
      this.clusterDetails = null;
    }
  }

  test(): void {
    console.log("test");
  }

  
}
