import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Cluster } from 'src/app/model/cluster';
import { ClusterDetails } from 'src/app/model/cluster-detail';
import { ApiClientService } from 'src/app/services/api-client.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-cluster-comparison',
  templateUrl: './cluster-comparison.component.html',
  styleUrls: ['./cluster-comparison.component.scss']
})
export class ClusterComparisonComponent implements OnInit, OnChanges {

  @Input() clusterDetails: ClusterDetails | null = null;

  @Input() allClusters: Cluster[] = [];

  selectedClusters: Cluster[] = [];

  selectedClustersDetails: ClusterDetails[] = [];

  constructor(private api: ApiClientService) { }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
  }

  assignDetails(event: any): void {
    this.selectedClustersDetails = []
    for(let cluster of this.selectedClusters) {
      this.api.getClusterDetails(cluster.name).subscribe(result => {
        let contains = false;
        for (let attribute of this.selectedClustersDetails)
        {
          if (attribute.name == result.name)
          {
            contains = true;
          }
        }

        if (!contains)
        {
          this.selectedClustersDetails.push(result);
        }        
      });
    }
  }

}
