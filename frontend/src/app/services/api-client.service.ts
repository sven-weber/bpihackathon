import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cluster } from '../model/cluster';
import { ClusterDetails } from '../model/cluster-detail';
import { AttributeDistribution } from '../model/attribute-distribution';

@Injectable({
  providedIn: 'root'
})
export class ApiClientService {

  private address : string = "https://hackathonapi.sven-weber.net";
  private apiToken : string = "hackathon";

  private dataSet = "MobIS";
  private epsilon: number  = 5;
  private minPts: number = 0;

  public minPtsChangeEvent = new EventEmitter();

  public epsilonChangeEvent = new EventEmitter();

  public dataSetChangeEvent = new EventEmitter();

  public connectionLevelChangeEvent = new EventEmitter();

  constructor(private httpClient: HttpClient) { }

  private getAuthHeader() : HttpHeaders {
    return new HttpHeaders().set('Token', this.apiToken);
  } 

  private getEndpoint(endpoint : string) : string {
    return `${this.address}/${endpoint}`;
  }

  public minPtsChanged(minPts: number | null) {
    console.log(minPts);
    if(minPts != null) { this.minPts = minPts; }
    this.minPtsChangeEvent.emit(this.minPts);
    console.log(`MinPts changed to ${this.minPts}`);
  }

  public epsilonChanged(epsilon: number | null) {
    if(epsilon != null) { this.epsilon = epsilon; }
    this.epsilonChangeEvent.emit(this.epsilon);
    console.log(`Epsilon changed to ${this.epsilon}`);
  }

  public dataSetChanged(dataSet: string) {
    this.dataSet = dataSet;
    this.dataSetChangeEvent.emit(this.dataSet);
    console.log(`Dataset changed to ${this.dataSet}`);
  }

  public connectionLevelChanged(connectionLevel: number | null) {
    console.log("connection level set to" + connectionLevel)
    this.connectionLevelChangeEvent.emit(connectionLevel);
  }

  /**
   * 
   * @param tableName 
   * @param minpk 
   * @param epsilon 
   * @returns 
   */
  public getClusters() : Observable<Cluster[]> {
    let param = { 'tableName': this.dataSet, 'minpk': this.minPts, 'epsilon': this.epsilon }; 
    return this.httpClient.get<Cluster[]>(this.getEndpoint('clusters'), { headers: this.getAuthHeader(), params: param });
  }

  public getMinPtsEstimation(): Observable<number[]> {
    let param = { 'tableName': this.dataSet, 'epsilon': this.epsilon};
    return this.httpClient.get<number[]>(this.getEndpoint('minimumpoints'), { headers: this.getAuthHeader(), params: param });
  }

  public getClusterDetails(clusterId : string): Observable<ClusterDetails> {
    let param = { 'tableName': this.dataSet, 'minpk': this.minPts, 'epsilon': this.epsilon };
    return this.httpClient.get<ClusterDetails>(this.getEndpoint(`clusters/${clusterId}`), { headers: this.getAuthHeader(), params: param });
  }

  public getAttributeDistribution(clusterId: string, attributeName: string): Observable<AttributeDistribution[]> {
    let param = { 'tableName': this.dataSet, 'minpk': this.minPts, 'epsilon': this.epsilon};
    return this.httpClient.get<AttributeDistribution[]>(this.getEndpoint(`clusters/${clusterId}/${attributeName}`), { headers: this.getAuthHeader(), params: param });
  }
  
}
