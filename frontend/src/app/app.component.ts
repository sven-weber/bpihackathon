import { numberToString } from '@amcharts/amcharts5/.internal/core/util/Type';
import { Component } from '@angular/core';
import { Cluster } from './model/cluster';
import { ApiClientService } from './services/api-client.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public epsilonDisplay: number | null = 5;
  public minPtsDisplay: number | null = null;
  public connectionLevelDisplay: number | null = 0;

  public epsilonValue: number | null = 5;
  public minPtsValue: number | null = null;
  public connectionLevelValue: number | null = 0;

  public selectedClusterId = "";
  public minPtsEstimations: number[] = [0,0,0,0];

  public clusters: Cluster[] = [];
  
  public color: string = "#FFFFFF";
  selectedDataSet: string = "MobIS";

  constructor(public api: ApiClientService) {


    this.api.getMinPtsEstimation().subscribe(result => {
      this.minPtsEstimations = result;
      this.minPtsValue = result.length-1;
      this.minPtsDisplay = result.length-1;
      this.api.minPtsChanged(this.minPtsEstimations[this.minPtsEstimations.length-1]);
    });

    this.api.dataSetChangeEvent.subscribe( () => {
      this.api.getMinPtsEstimation().subscribe(result => {
        this.minPtsEstimations = result;
        this.minPtsValue = result.length-1;
        this.minPtsDisplay = result.length-1;
        this.api.minPtsChanged(this.minPtsEstimations[this.minPtsEstimations.length-1]);
      });
    });  
  

    this.api.epsilonChangeEvent.subscribe( () => {
      this.api.getMinPtsEstimation().subscribe(result => {
        this.minPtsEstimations = result;
        this.minPtsValue = result.length-1;
        this.minPtsDisplay = result.length-1;
        this.api.minPtsChanged(this.minPtsEstimations[result.length-1]);
      });
    })
  }

  getMinPtsEstimationAtMinPtsDisplay() {
    if(this.minPtsDisplay !== null) {
      return this.minPtsEstimations[this.minPtsDisplay];
    } else {
      return 0; 
    }
  }

  minPtsValueChange() {
    if(this.minPtsValue !== null) {
      this.api.minPtsChanged(this.minPtsEstimations[this.minPtsValue]);
    }
  }


}
