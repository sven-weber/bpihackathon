module.exports = {
  important: true,
  content: ['./src/**/*.{html,ts,js}'],
  theme: {
    extend: {},
  },
  plugins: [
    require('tailwind-scrollbar'),
  ],
}
